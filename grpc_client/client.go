package grpcclient

import (
	"fmt"
	"sync"

	"gitlab.com/test/api_gateway/config"
	fetch_service "gitlab.com/test/api_gateway/protos/fetch_service"
	post_service "gitlab.com/test/api_gateway/protos/post_service"
	"google.golang.org/grpc"
)

var cfg = config.Get()
var (
	oncePostService      sync.Once
	instancePostService  post_service.PostServiceClient
	onceFetchService     sync.Once
	instanceFetchService fetch_service.FetchServiceClient
)

// PostService ...
func PostService() post_service.PostServiceClient {
	oncePostService.Do(func() {
		connPost, err := grpc.Dial(
			fmt.Sprintf("%s:%d", cfg.PostServiceHost, cfg.PostServicePort),
			grpc.WithInsecure())
		if err != nil {
			panic(fmt.Errorf("post service dial host: %s port:%d err: %s",
				cfg.PostServiceHost, cfg.PostServicePort, err))
		}

		instancePostService = post_service.NewPostServiceClient(connPost)
	})

	return instancePostService
}

// FetchService ...
func FetchService() fetch_service.FetchServiceClient {
	onceFetchService.Do(func() {
		connFetch, err := grpc.Dial(
			fmt.Sprintf("%s:%d", cfg.FetchServiceHost, cfg.FetchServicePort),
			grpc.WithInsecure())
		if err != nil {
			panic(fmt.Errorf("Fetch service dial host: %s port:%d err: %s",
				cfg.FetchServiceHost, cfg.FetchServicePort, err))
		}

		instanceFetchService = fetch_service.NewFetchServiceClient(connFetch)
	})

	return instanceFetchService
}
