package handlers

import (
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/test/api_gateway/config"
	grpc_client "gitlab.com/test/api_gateway/grpc_client"
	fetch_pb "gitlab.com/test/api_gateway/protos/fetch_service"
)

var conf = *config.Get()

func (h *Handler) FetchPosts(c *gin.Context) {

	pageStr, exists := c.Params.Get("page")

	if !exists {
		h.logger.Println("Error getting post err:", errors.New("Param id not given"))
		c.JSON(http.StatusInternalServerError, response{Error: true, Message: "Param id not given"})
		return
	}

	page, err := strconv.Atoi(pageStr)
	if err != nil {
		h.logger.Println("Error parsing query parametr err:", err)
		c.JSON(http.StatusBadRequest, response{Error: true, Message: err.Error()})
		return
	}
	request := fetch_pb.FetchPostRequest{Page: uint32(page)}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(conf.CtxTimeOut))
	defer cancel()

	_, err = grpc_client.FetchService().Fetch(ctx, &request)
	if err != nil {
		h.logger.Println("Error feching post err:", err)
		c.JSON(http.StatusInternalServerError, response{Error: true, Message: err.Error()})
		return
	}

	c.JSON(http.StatusOK, response{Error: false, Message: "Success"})
	return
}
