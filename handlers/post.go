package handlers

import (
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	grpc_client "gitlab.com/test/api_gateway/grpc_client"
	post_pb "gitlab.com/test/api_gateway/protos/post_service"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/gin-gonic/gin"
)

func (h *Handler) ListPosts(c *gin.Context) {

	pageStr := c.Query("page")
	limitStr := c.Query("limit")

	page, err := strconv.Atoi(pageStr)
	if pageStr != "" && err != nil {
		h.logger.Println("Error parsing query parametr err:", err)
		c.JSON(http.StatusBadRequest, response{Error: true, Message: "Error parsing page"})
		return
	}

	limit, err := strconv.Atoi(limitStr)
	if limitStr != "" && err != nil {
		h.logger.Println("Error parsing query parametr err:", err)
		c.JSON(http.StatusBadRequest, response{Error: true, Message: "Error parsing limit"})
		return
	}

	if page == 0 {
		page = 1
	}
	if limit == 0 {
		limit = 10
	}

	request := post_pb.ListPostRequest{Limit: uint64(limit), Page: uint64(page)}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(conf.CtxTimeOut))
	defer cancel()

	res, err := grpc_client.PostService().List(ctx, &request)
	if err != nil {
		h.logger.Println("Error listing post err:", err)
		c.JSON(http.StatusInternalServerError, response{Error: true, Message: "Internal server error"})
		return
	}

	c.JSON(http.StatusOK, res)
	return
}

func (h *Handler) GetPost(c *gin.Context) {

	idStr, exists := c.Params.Get("id")

	if !exists {
		h.logger.Println("Error getting post err:", errors.New("Param id not given"))
		c.JSON(http.StatusBadRequest, response{Error: true, Message: "Param id not given"})
		return
	}

	id, err := strconv.Atoi(idStr)
	if err != nil {
		h.logger.Println("Error parsing query parametr err:", err)
		c.JSON(http.StatusBadRequest, response{Error: true, Message: "Param id not given"})
		return
	}

	request := post_pb.GetPostRequest{Id: uint64(id)}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(conf.CtxTimeOut))
	defer cancel()

	res, err := grpc_client.PostService().Get(ctx, &request)
	if status.Code(err) == codes.NotFound {
		h.logger.Println("Error getting post err:", err)
		c.JSON(http.StatusBadRequest, response{Error: true, Message: "Not found"})
		return
	} else if err != nil {
		h.logger.Println("Error getting post err:", err)
		c.JSON(http.StatusInternalServerError, response{Error: true, Message: "Internal server error"})
		return
	}

	c.JSON(http.StatusOK, res)
	return
}

type postCreateReq struct {
	Title  string `json:"title" required:"true"`
	Body   string `json:"body" required:"true"`
	UserId uint32 `json:"user_id" required:"true"`
}

func (h *Handler) CreatePost(c *gin.Context) {

	req := postCreateReq{}
	err := c.ShouldBind(&req)
	if err != nil {
		h.logger.Println("Error binding json err:", err)
		c.JSON(http.StatusBadRequest, response{Error: true, Message: "Error binding json"})
		return
	}

	if req.Body == "" || req.Title == "" || req.UserId == 0 {
		h.logger.Println("Error binding json err:", err)
		c.JSON(http.StatusBadRequest, response{Error: true, Message: "Not proper input"})
		return
	}
	request := post_pb.CreatePostRequest{
		Post: &post_pb.Post{
			Title:  req.Title,
			Body:   req.Body,
			UserId: req.UserId,
		},
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(conf.CtxTimeOut))
	defer cancel()

	res, err := grpc_client.PostService().Create(ctx, &request)
	if err != nil {
		h.logger.Println("Error creating post err:", err)
		c.JSON(http.StatusInternalServerError, response{Error: true, Message: "Internal server error"})
		return
	}

	c.JSON(http.StatusOK, res)
	return
}

func (h *Handler) UpdatePost(c *gin.Context) {

	req := &post_pb.Post{}
	err := c.ShouldBind(&req)
	if err != nil {
		h.logger.Println("Error binding json err:", err)
		c.JSON(http.StatusBadRequest, response{Error: true, Message: "Error binding json"})
		return
	}

	if (req.Body == "" && req.Title == "") || req.Id == 0 || req.UserId == 0 {
		h.logger.Println("Error binding json err:", err)
		c.JSON(http.StatusBadRequest, response{Error: true, Message: "Not proper input"})
		return
	}
	request := post_pb.UpdatePostRequest{
		Post: req,
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(conf.CtxTimeOut))
	defer cancel()

	_, err = grpc_client.PostService().Update(ctx, &request)
	if status.Code(err) == codes.NotFound {
		h.logger.Println("Error updating post err:", err)
		c.JSON(http.StatusBadRequest, response{Error: true, Message: "Not found"})
		return
	} else if err != nil {
		h.logger.Println("Error updating post err:", err)
		c.JSON(http.StatusInternalServerError, response{Error: true, Message: "Internal server error"})
		return
	}

	c.JSON(http.StatusOK, response{Error: false, Message: "Success"})
	return
}

func (h *Handler) DeletePost(c *gin.Context) {

	idStr, exists := c.Params.Get("id")

	if !exists {
		h.logger.Println("Error getting post err:", errors.New("Param id not given"))
		c.JSON(http.StatusInternalServerError, response{Error: true, Message: "Param id not given"})
		return
	}

	id, err := strconv.Atoi(idStr)
	if err != nil {
		h.logger.Println("Error parsing query parametr err:", err)
		c.JSON(http.StatusBadRequest, response{Error: true, Message: "Error parsing query parametr"})
		return
	}

	request := post_pb.DeletePostRequest{Id: uint64(id)}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(conf.CtxTimeOut))
	defer cancel()

	_, err = grpc_client.PostService().Delete(ctx, &request)
	if status.Code(err) == codes.NotFound {
		h.logger.Println("Error deleting post err:", err)
		c.JSON(http.StatusBadRequest, response{Error: true, Message: "Not found"})
		return
	} else if err != nil {
		h.logger.Println("Error deleting post err:", err)
		c.JSON(http.StatusInternalServerError, response{Error: true, Message: "Internal server error"})
		return
	}

	c.JSON(http.StatusOK, response{Error: false, Message: "Success"})
	return
}
