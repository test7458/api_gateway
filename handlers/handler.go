package handlers

import "log"

type Handler struct {
	logger *log.Logger
}

func New(l *log.Logger) *Handler {
	return &Handler{logger: l}
}

type response struct {
	Error   bool   `json:"error"`
	Message string `json:"message"`
}
