package main

import (
	"log"

	"github.com/gin-gonic/gin"
	_ "github.com/joho/godotenv/autoload" // load .env file automatically
	"gitlab.com/test/api_gateway/handlers"
	"go.uber.org/zap"

	_ "github.com/lib/pq"
)

func main() {
	// =========================================================================
	// Config
	// conf := config.Get()

	ginEngine := gin.Default()

	logger := log.Default()
	logger.SetPrefix("api_gateway:")
	h := handlers.New(logger)
	r := ginEngine.Group("/api")
	// r.Use(gin.Logger())

	r.GET("/posts", h.ListPosts)
	r.GET("/posts/:id", h.GetPost)
	r.POST("/posts", h.CreatePost)
	r.DELETE("/posts/:id", h.DeletePost)
	r.PATCH("/posts", h.UpdatePost)
	r.POST("/fetch-posts/:page", h.FetchPosts)

	if err := ginEngine.Run(); err != nil {
		logger.Fatal("error while running gin server", zap.Error(err))
	}
}
