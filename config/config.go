package config

import (
	"os"
	"sync"

	_ "github.com/joho/godotenv/autoload" // load .env file automatically

	"github.com/spf13/cast"
)

//Config ...
type Config struct {
	HTTPPort         string
	PostServiceHost  string
	PostServicePort  int
	FetchServiceHost string
	FetchServicePort int
	CtxTimeOut       int
}

func load() *Config {
	return &Config{
		HTTPPort:         cast.ToString(getOrReturnDefault("HTTP_PORT", ":8000")),
		PostServiceHost:  cast.ToString(getOrReturnDefault("POST_SERVICE_HOST", "localhost")),
		PostServicePort:  cast.ToInt(getOrReturnDefault("POST_SERVICE_PORT", 9000)),
		FetchServiceHost: cast.ToString(getOrReturnDefault("FETCh_SERVICE_HOST", "localhost")),
		FetchServicePort: cast.ToInt(getOrReturnDefault("FETCH_SERVICE_PORT", 9001)),
		CtxTimeOut:       cast.ToInt(getOrReturnDefault("CTX_TIMEOUT", 10)),
	}
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}

var (
	instance *Config
	once     sync.Once
)

//Get ...
func Get() *Config {
	once.Do(func() {
		instance = load()
	})

	return instance
}
